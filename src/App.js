import './App.css';
import Nav from './Nav'
import AppRoutes from './AppRoutes'
import Calculate from './components/Calculate'
import Counter from './components/Counter'

function App() {
  return (
    <div className="App">
      <Calculate />
      <Counter />
      {/* <Nav />
      <hr />
      <AppRoutes /> */}
    </div>
  );
}

export default App;
