import { Link } from "react-router-dom"

import React from 'react'

const Nav = () => {
    return (
        <Nav>
            <ul>
                <li><Link to="/">Counter</Link></li>
                <li><Link to="/calculate">Calculate</Link></li>
            </ul>
        </Nav>
    )
}

export default Nav
