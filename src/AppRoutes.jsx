import Calculate from './components/Calculate'
import Counter from './components/Counter'
import { Switch, Route } from "react-router-dom"

const AppRoutes = () => {
    return (
        <div>
            <Switch>
                <Route exact path="/">
                    <Counter />
                </Route>
                <Route exact path="/calculate">
                    <Calculate />
                </Route>
            </Switch>
        </div>
    )
}

export default AppRoutes
